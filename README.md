# README #

1/3スケールのバンダイ RX-78風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK FUSION360です。


***

# 実機情報

## メーカ
- バンダイ

## 発売時期
- 1983年7月

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/RX-78_(%E3%83%91%E3%82%BD%E3%82%B3%E3%83%B3))

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_rx-78/raw/278148e6ea3dc81834ac239ac3e684c6f5aff2be/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_rx-78/raw/278148e6ea3dc81834ac239ac3e684c6f5aff2be/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_rx-78/raw/278148e6ea3dc81834ac239ac3e684c6f5aff2be/ExampleImage.jpg)
